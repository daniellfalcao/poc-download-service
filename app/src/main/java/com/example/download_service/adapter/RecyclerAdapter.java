package com.example.download_service.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.download_service.R;
import com.example.download_service.item.DownloadItemView;
import com.example.download_service.model.Download;
import com.example.download_service.model.FileInfo;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.List;

import static com.example.download_service.model.Download.STATUS_DOWNLOADED;
import static com.example.download_service.model.Download.STATUS_INITIAL;

@EBean
public class RecyclerAdapter extends RecyclerView.Adapter<DownloadItemView> {

    public List<Download> itens = new ArrayList<>();

    @AfterInject
    void afterInject() {
        itens.add(new Download(new FileInfo(1), STATUS_INITIAL, false ));
        itens.add(new Download(new FileInfo(2), STATUS_DOWNLOADED, false ));
        itens.add(new Download(new FileInfo(1), STATUS_INITIAL, false ));
        itens.add(new Download(new FileInfo(4), STATUS_INITIAL, false ));
        itens.add(new Download(new FileInfo(5), STATUS_DOWNLOADED, false ));
        itens.add(new Download(new FileInfo(6), STATUS_DOWNLOADED, false ));
        itens.add(new Download(new FileInfo(7), STATUS_INITIAL, false ));
        itens.add(new Download(new FileInfo(8), STATUS_INITIAL, false ));
        itens.add(new Download(new FileInfo(9), STATUS_INITIAL, false ));
        itens.add(new Download(new FileInfo(10), STATUS_INITIAL, false ));
        itens.add(new Download(new FileInfo(11), STATUS_DOWNLOADED, false ));
        itens.add(new Download(new FileInfo(12), STATUS_INITIAL, false ));
        itens.add(new Download(new FileInfo(13), STATUS_DOWNLOADED, false ));
        itens.add(new Download(new FileInfo(14), STATUS_INITIAL, false ));
        itens.add(new Download(new FileInfo(15), STATUS_DOWNLOADED, false ));


        notifyDataSetChanged();
    }

    @Override
    public DownloadItemView onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DownloadItemView(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_download, parent, false));
    }

    @Override
    public void onBindViewHolder(DownloadItemView holder, int position) {
        holder.bind(itens.get(position));
    }

    @Override
    public int getItemCount() {
        return itens.size() ;
    }
}
