package com.example.download_service.model;


public interface DownloadServiceCallback {

    void onProgressChanged(int currentProgress, int id);
    void onDownloadStarted(int contentLength, int id);
    void onDownloadFinished(int id);
    void onError(int id);

}
