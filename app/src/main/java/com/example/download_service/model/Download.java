package com.example.download_service.model;

/**
 * Created by Daniel on 10/02/2018.
 */

public class Download {

    public static final int STATUS_INITIAL = 0;
    public static final int STATUS_DOWNLOADED = 1;
    public static final int STATUS_DOWNLOADING = 2;
    public static final int STATUS_QUEUE = 4;

    private FileInfo fileInfo;
    private int downloadStatus;
    private boolean isConfigured;

    public Download(FileInfo fileInfo, int downloadStatus, boolean isConfigured) {
        this.fileInfo = fileInfo;
        this.downloadStatus = downloadStatus;
        this.isConfigured = isConfigured;
    }

    public FileInfo getFileInfo() {
        return fileInfo;
    }

    public void setFileInfo(FileInfo fileInfo) {
        this.fileInfo = fileInfo;
    }

    public int getDownloadStatus() {
        return downloadStatus;
    }

    public void setDownloadStatus(int downloadStatus) {
        this.downloadStatus = downloadStatus;
    }

    public boolean isConfigured() {
        return isConfigured;
    }

    public void setConfigured(boolean isConfigured) {
        this.isConfigured = isConfigured;
    }
}
