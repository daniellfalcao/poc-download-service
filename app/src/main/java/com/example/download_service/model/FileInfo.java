package com.example.download_service.model;

/**
 * Created by Daniel on 11/02/2018.
 */

public class FileInfo {

    private int id;
    private String name;

    public FileInfo(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
