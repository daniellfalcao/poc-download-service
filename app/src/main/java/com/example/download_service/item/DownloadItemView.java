package com.example.download_service.item;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.download_service.R;
import com.example.download_service.model.Download;
import com.example.download_service.model.DownloadServiceCallback;
import com.example.download_service.service.DownloadService;

import static com.example.download_service.service.DownloadService.INTENT_DOWNLOAD_ID;
import static com.example.download_service.service.DownloadService.INTENT_DOWNLOAD_LENGTH;
import static com.example.download_service.service.DownloadService.INTENT_DOWNLOAD_PROGRESS;
import static com.example.download_service.service.DownloadService.INTENT_ON_DOWNLOAD_FINISHED;
import static com.example.download_service.service.DownloadService.INTENT_ON_DOWNLOAD_STARTED;
import static com.example.download_service.service.DownloadService.INTENT_ON_ERROR;
import static com.example.download_service.service.DownloadService.INTENT_ON_PROGRESS_CHANGED;


public class DownloadItemView extends RecyclerView.ViewHolder implements DownloadServiceCallback{

    private ProgressBar progressBar;
    private ImageView buttonDownload;

    private Download download;

    public DownloadItemView(View itemView) {
        super(itemView);
    }

    public void bind(final Download download) {
        this.download = download;

        progressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);
        buttonDownload = (ImageView) itemView.findViewById(R.id.view_download);

        configBroadcastReceiver();

        buttonDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            Intent intent = new Intent(itemView.getContext(),DownloadService.class);
            intent.putExtra(INTENT_DOWNLOAD_ID, download.getFileInfo().getId());
            itemView.getContext().startService(intent);

            }
        });

        switch (download.getDownloadStatus()) {
            case Download.STATUS_INITIAL:
                configStatusInitial();
                break;
            case Download.STATUS_DOWNLOADED:
                configStatusDownloaded();
                break;
            case Download.STATUS_DOWNLOADING:
                configStatusDownloading();
                break;
        }

    }

    private void configStatusDownloading() {
        download.setDownloadStatus(Download.STATUS_DOWNLOADING);
        progressBar.setVisibility(View.VISIBLE);
        buttonDownload.setEnabled(false);
        buttonDownload.setImageDrawable(itemView.getContext().getResources().getDrawable(R.drawable.ic_download));
    }

    private void configStatusDownloaded() {
        download.setDownloadStatus(Download.STATUS_DOWNLOADED);
        progressBar.setVisibility(View.INVISIBLE);
        buttonDownload.setEnabled(false);
        buttonDownload.setImageDrawable(itemView.getContext().getResources().getDrawable(R.drawable.ic_downloaded));
    }

    private void configStatusInitial() {
        download.setDownloadStatus(Download.STATUS_INITIAL);
        progressBar.setVisibility(View.INVISIBLE);
        buttonDownload.setEnabled(true);
        buttonDownload.setImageDrawable(itemView.getContext().getResources().getDrawable(R.drawable.ic_download));
    }

    public void configBroadcastReceiver() {

        if (download.isConfigured()) {
            return;
        }

        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(itemView.getContext());

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(INTENT_ON_DOWNLOAD_STARTED);
        intentFilter.addAction(INTENT_ON_DOWNLOAD_FINISHED);
        intentFilter.addAction(INTENT_ON_PROGRESS_CHANGED);
        intentFilter.addAction(INTENT_ON_ERROR);

        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent != null && intent.getAction() != null && intent.getExtras() != null) {

                    Bundle extra = intent.getExtras();

                    if (extra.getInt(INTENT_DOWNLOAD_ID) != download.getFileInfo().getId()){
                        return;
                    }

                    if (intent.getAction().equals(INTENT_ON_DOWNLOAD_STARTED)) {

                        onDownloadStarted(extra.getInt(INTENT_DOWNLOAD_LENGTH),
                                extra.getInt(INTENT_DOWNLOAD_ID));

                    } else if (intent.getAction().equals(INTENT_ON_DOWNLOAD_FINISHED)) {

                        onDownloadFinished(extra.getInt(INTENT_DOWNLOAD_ID));

                    } else if (intent.getAction().equals(INTENT_ON_PROGRESS_CHANGED)) {

                        onProgressChanged(extra.getInt(INTENT_DOWNLOAD_PROGRESS),
                                extra.getInt(INTENT_DOWNLOAD_ID));

                    } else if (intent.getAction().equals(INTENT_ON_ERROR)) {

                        onError(extra.getInt(INTENT_DOWNLOAD_ID));

                    }
                }
            }
        };

        bManager.registerReceiver(broadcastReceiver, intentFilter);

        download.setConfigured(true);
    }


    @Override
    public synchronized void onProgressChanged(int currentProgress, int id) {
        configStatusDownloading();
        progressBar.setProgress(currentProgress);
    }

    @Override
    public void onDownloadStarted(int contentLength, int id) {
        configStatusDownloading();
        progressBar.setMax(contentLength);
    }

    @Override
    public void onDownloadFinished(int id) {
        configStatusDownloaded();
    }

    @Override
    public void onError(int id) {
        configStatusInitial();
    }
}
