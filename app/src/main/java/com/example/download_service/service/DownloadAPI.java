package com.example.download_service.service;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Streaming;

public interface DownloadAPI {

    @GET("downloadMusic?key=L4oTeDVy&amp;time=")
    @Streaming
    Call<ResponseBody> downloadFile();
}
