package com.example.download_service.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.download_service.model.DownloadServiceCallback;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;

public class DownloadService extends IntentService implements DownloadServiceCallback {

    private final static String TAG = "DownloadService";

    public final static String INTENT_ADD_DOWNLOAD = "intent_add_download";
    public final static String INTENT_ON_DOWNLOAD_STARTED = "intent_start_download";
    public final static String INTENT_ON_DOWNLOAD_FINISHED = "intent_finish_download";
    public final static String INTENT_ON_PROGRESS_CHANGED = "intent_progress_changed";
    public final static String INTENT_ON_ERROR = "intent_error";

    public final static String INTENT_DOWNLOAD_ID = "intent_download_id";
    public final static String INTENT_DOWNLOAD_PROGRESS = "intent_download_progress";
    public final static String INTENT_DOWNLOAD_LENGTH = "intent_download_length";

    public final static String STATUS_DOWNLOADING = "status_downloading";
    public final static String STATUS_QUEUE = "status_queue";

    private DownloadAPI API;

    public DownloadService() {
        super("Download Service");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        Log.d(TAG, "onHandleIntent() called with: intent = [" + intent + "]");

        if(intent != null && intent.getExtras() != null){
            startDownload(intent.getExtras().getInt(INTENT_DOWNLOAD_ID));
        }
    }

    @Override
    public void onProgressChanged(final int currentProgress, final int id) {

        Log.d(TAG, "onProgressChanged() called with: currentProgress = [" + currentProgress + "], id = [" + id + "]");

        Intent intent = new Intent(INTENT_ON_PROGRESS_CHANGED);
        intent.putExtra(INTENT_DOWNLOAD_PROGRESS, currentProgress);
        intent.putExtra(INTENT_DOWNLOAD_ID, id);

        LocalBroadcastManager.getInstance(DownloadService.this).sendBroadcast(intent);
    }

    @Override
    public void onDownloadStarted(final int contentLength, final int id) {

        Log.d(TAG, "onDownloadStarted() called with: contentLength = [" + contentLength + "], id = [" + id + "]");

        Intent intent = new Intent(INTENT_ON_DOWNLOAD_STARTED);
        intent.putExtra(INTENT_DOWNLOAD_LENGTH, contentLength);
        intent.putExtra(INTENT_DOWNLOAD_ID, id);

        LocalBroadcastManager.getInstance(DownloadService.this).sendBroadcast(intent);
    }

    @Override
    public void onDownloadFinished(final int id) {

        Log.d(TAG, "onDownloadFinished() called with: id = [" + id + "]");

        Intent intent = new Intent(INTENT_ON_DOWNLOAD_FINISHED);
        intent.putExtra(INTENT_DOWNLOAD_ID, id);

        LocalBroadcastManager.getInstance(DownloadService.this).sendBroadcast(intent);
    }

    @Override
    public void onError(final int id) {

        Log.d(TAG, "onError() called with: id = [" + id + "]");

        Intent intent = new Intent(INTENT_ON_ERROR);
        intent.putExtra(INTENT_DOWNLOAD_ID, id);

        LocalBroadcastManager.getInstance(DownloadService.this).sendBroadcast(intent);
    }

    private void createRetrofitInstance() {
        if (API == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://www43.zippyshare.com/")
                    .build();
            API = retrofit.create(DownloadAPI.class);
        }
    }

    private void startDownload(final int downloadID){

        Log.d(TAG, "startDownload() called");

        createRetrofitInstance();

        new Thread(requestDownload(downloadID)).start();
    }

    private Runnable requestDownload(final int downloadID) {
        return new Runnable() {
            @Override
            public void run() {
                try {

                    Call<ResponseBody> request = API.downloadFile();
                    boolean writtenToDisk = writeResponseBodyToDisk(request.execute().body(), downloadID);

                    if (!writtenToDisk) {
                        onError(downloadID);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    onError(downloadID);
                }
            }
        };
    }

    private boolean writeResponseBodyToDisk(ResponseBody body, final int downloadID) {

        try {

            //File audioFile = new File(getExternalFilesDir(null) + File.separator + "audio.mp3");
            File audioFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "audio.mp3");

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;
                long startedTime = System.currentTimeMillis();

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(audioFile);

                onDownloadStarted((int)body.contentLength(), downloadID);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                    if(System.currentTimeMillis() - startedTime > 250) {
                        onProgressChanged((int) fileSizeDownloaded, downloadID);
                        startedTime = System.currentTimeMillis();
                    }

                    Log.d(TAG, "file download: " + fileSizeDownloaded + " of " + fileSize);
                }

                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }

                onDownloadFinished(downloadID);
            }
        } catch (IOException e) {
            return false;
        }
    }

}

